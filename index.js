const puppeteer = require('puppeteer'); // v 1.1.0
const $ = require('cheerio');
const fs = require('fs');
const request = require('request');

let url = 'https://www.travel-to-milos.com'

const download = function(uri, filename, callback){
    request.head(uri, function(err, res, body){
        request(uri).pipe(fs.createWriteStream(filename)).on('close', callback);
    });
};

async function scrap() {

    const browser = await puppeteer.launch({headless: false});
    const page = await browser.newPage();
    await page.goto(url);

    await page.evaluate(() => {
        scroll(0, 99999)
    });
    await page.waitFor(1000);

    let  bodyHTML = await page.evaluate(() => document.body.innerHTML);

    //Regex for Links
    let link_l_reg = /links_l/g;

    let Pages = [];

    $( 'table' , bodyHTML ).each(function () {

        let links = $( this , bodyHTML ).html().match(  link_l_reg  )

        if( links !== null){
            let reviewText = $(this).find("a." + links[0]).attr('href')
            $(this).find("a." + links[0]).each(function(){

                let name = $(this).text().replace(/\s/g, '')
                let text = $(this).attr('href')
                let obj = {
                    name : name,
                    link : text
                }
                Pages.push(obj)
            })
        }

    })

    const PagesArr = Pages.filter((item,index) => {
        return index === Pages.findIndex(obj => {
            return JSON.stringify(obj) === JSON.stringify(item);
        });
    });

    console.log(PagesArr)

    let AllPages = []
    for ( let i = 0; i < PagesArr.length; i++){

        let Hotel_Pages = []
        const page = await browser.newPage();
        let newPage = url + PagesArr[i].link
        await page.goto(newPage);
        await page.evaluate(() => {
            scroll(0, 99999)
        });
        await page.waitFor(1000);

        let link__reg = /links_cont/g;

        let bodyHTML = await page.evaluate(() => document.body.innerHTML);

        $('table', bodyHTML).each(function () {

            let links = $(this, bodyHTML).html().match(link__reg)

            if (links !== null) {

                $(this).find("a." + links[0]).each(function () {

                    let name = $(this).text().replace(/\s/g, '')
                    let text = $(this).attr('href')
                    let obj = {
                        name: name,
                        link: text
                    }
                    Hotel_Pages.push(obj)
                })
            }

        })

        const Hotel_Pages_Arr = Hotel_Pages.filter((item, index) => {
            return index === Hotel_Pages.findIndex(obj => {
                return JSON.stringify(obj.link) === JSON.stringify(item.link);
            });
        }).slice(1);

        let oo = {
            page: PagesArr[i].name,
            oo: Hotel_Pages_Arr
        }

        AllPages.push(oo)
        console.log(Hotel_Pages_Arr)

        await page.close()

    }

    //Unique arrays and remove first Item
    console.log("1")
    console.log(AllPages)
    console.log(AllPages[1].oo)

    for( let i = 0; i < AllPages.length; i++){
        if( i == 1 ){

            for( let x of AllPages[i].oo){
                console.log(x.name)

                fs.existsSync('./static/' + x.name) || fs.mkdirSync('./static/' + x.name);

                const page = await browser.newPage();
                let newPage = url + x.link
                await page.goto(newPage);
                await page.evaluate(() => {
                    scroll(0, 99999)
                });
                await page.waitFor(1000);

                let link__reg = /myfancybox/g;
                let text__reg = /ui-widget-header/g;

                let  bodyHTML = await page.evaluate(() => document.body.innerHTML);

                $( 'table' , bodyHTML ).each(function () {

                    let TEXTS = $( this , bodyHTML ).html().match(  text__reg  )
                    console.log("eimai to " + TEXTS)
                    if( TEXTS !== null) {

                        let tex = $(this).find("." + TEXTS[0]).parent().text().replace(/\s+/g, ' ')

                        let strData =  './static/' + x.name + '/Data.json'

                        fs.writeFile( strData ,  JSON.stringify(tex) , 'utf8',(err)=> {
                            console.log("change data output")
                            return (err) ? console.log(err): console.log("The file pages was saved!");
                        });

                    }

                    let links = $( this , bodyHTML ).html().match(  link__reg  )

                    if( links !== null){

                        $(this).find("a." + links[0]).each(function(){

                            // let name = $(this).text().replace(/\s/g, '')
                            let link = $(this).attr('href')

                            let img = link.split('/')

                            let ImgsUrls = './static/' + x.name + '/'  + img[img.length-1];

                            console.log(ImgsUrls)
                            download(link , ImgsUrls , function(){
                                console.log('done');
                            });

                        })

                    }

                })

            }

        }
    }

}

scrap()
